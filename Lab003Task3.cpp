#include <stdio.h>
#define max 255

int ItSpace(char);

char string[max];


int main()
{
	int flag=0,index=0,counter=0,len=0;
	int maxword=0,maxlen=0,begin=0;
	char sym;

	printf("Enter the string: ");
	gets(string);
	
	while(index<max && string[index])
	{

		sym = string[index];

		if(flag == 0 && ItSpace(sym) == 0)
		{
			flag = 1;
			len = 1;
			counter++;
			begin = index;

		}
		else if(flag && ItSpace(sym))
		{
			flag = 0;

			if(len>maxlen)
			{
				maxlen=len;
				maxword = begin;
			}
			
		}
		else if(flag && ItSpace(sym) == 0)
		{
			len++;
		};

		index++;
	};

	if(flag && (len>maxlen))
	{
		maxlen=len;
		maxword = begin;
	};

	if(maxlen)
	{
		string[maxword+maxlen] = 0;
		printf("Word <%s> is longest. It have %d symbols.\n", string+maxword, maxlen);
	}
	else
		printf("No words\n");

	return 0;
}

int ItSpace(char sym)
{
	int ErrCode = 0;

	if( sym == ' ' || sym == 13 )
		ErrCode = 1;

	return ErrCode;
}