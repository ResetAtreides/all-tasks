#include <stdio.h>
#include <math.h>

int main()
{
	char gender[10];
	int weight,normalweight,growth;
	char Male[] = "male";
	char Female[] = "Female";
	char Fat[] = "lose";
	char Thin[] = "gain";

	do {

		printf("\nEnter your gender(male/female): "); 
		fflush(stdin);
		scanf("%s",&gender);

		}while (*gender != 'm' && *gender != 'f');

	do {

		printf("\nEnter your weight: "); 
		fflush(stdin);
		scanf("%d",&weight);

		}while (weight < 30 || weight > 500);
	
	do {

		printf("\nEnter your growth: "); 
		fflush(stdin);
		scanf("%d",&growth);

		}while (growth < 30 || growth > 300);

	printf("You are %s. Your weight: %d, you growth: %d\n", (*gender == 'm'?Male:Female), weight, growth);

	normalweight = growth - (*gender == 'm'?100:110);

	if (abs(weight - normalweight) <= 5)
		printf("Normal weight\n");
	else
		printf("You need to %s weight\n", (normalweight > weight?Thin:Fat));

	return 0;
}

