#include <stdio.h>

int main()
{
	int ft,inch;
	float s;

	printf("Enter ft,inch: ");
	fflush(stdin);

	scanf("%d,%d", &ft, &inch);

	s = (ft*12 + inch)*2.54;

	printf("%d ft %d inch = %.1f sm\n", ft, inch, s);

	return 0;
}