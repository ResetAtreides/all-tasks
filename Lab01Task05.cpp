#include <stdio.h>
#include <string.h>

int main()
{
	char string[255];

	printf("Enter the string: ");
	gets(string);

	printf("%*s\n", 40 + strlen(string)/2, string);

	return 0;
}