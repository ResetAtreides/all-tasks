#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct Unit
{
	unsigned char Ch;
	int Count;
	int Basic;
	float freq;
	char code[255];
	struct Unit *left;
	struct Unit *right;
};
union CODE {
	unsigned char ch;
	struct {
		unsigned short b1:1;
		unsigned short b2:1;
		unsigned short b3:1;
		unsigned short b4:1;
		unsigned short b5:1;
		unsigned short b6:1;
		unsigned short b7:1;
		unsigned short b8:1;
	} byte;
};
int CheckParemeters(int CountParameters);
int OpenFile(FILE** F_Input, char* FileNameInput, FILE** F_Output, char* FileNameOutput);
int ScanFile(FILE *F_Input, struct Unit FileSize[256]);
void ClearStat(struct Unit Statistic[256]);
void SortStatistic(struct Unit Statistic[256], struct Unit *Pointers[256]);
void SwapElements(struct Unit Statistic[256], int index1, int index2);
struct Unit *BuildTree(struct Unit *Pointers[256], int index);
void InsertElement(struct Unit *Pointers[256], struct Unit *Element, int MaxIndex);
void MakeCodes(struct Unit *root);
void SaveFile001(FILE *F_Input, FILE* F_Output, struct Unit Statistic[256]);
void SavePackFile(FILE *F_Input, FILE* F_Output);
int FileSize(FILE *File);
void SaveHeader(FILE* F_Output, char *Header);
void SaveStatistics(FILE* F_Output, struct Unit Statistic[256]);
void SaveEndBits(FILE* F_Output, FILE* F_Input);
void SaveOriginalSize(FILE* F_Output, int *Size);
void SaveOriginalExtension(FILE* F_Output, char *Extension);
void ReadOriginalExtension(char *OriginalExtension, char *argv);
void FreeMemory(struct Unit *root);
int main(int argc, char* argv[])
{
	struct Unit Statistic[256];
	struct Unit * Pointers[256];
	struct Unit *root;
	FILE *F_Input=NULL;
	FILE *F_Output=NULL;
	char FileName001[] = "temp.001";
	char Header[] = "HFF";
	char OriginalExtension[] = "   ";
	int OriginalFileSize;
	int EndBits;

	//Check files
	if(!CheckParemeters(argc) || !OpenFile(&F_Input, argv[1], &F_Output, FileName001))
		return 1;

	ClearStat(Statistic);
	ReadOriginalExtension(OriginalExtension, argv[1]);
	OriginalFileSize = ScanFile(F_Input, Statistic);
	if(!OriginalFileSize)
		return 1;


	SortStatistic(Statistic, Pointers);
	root = BuildTree(Pointers, 253);
	MakeCodes(root);
	rewind(F_Input);
	SaveFile001(F_Input, F_Output, Statistic);
	fcloseall();

	//Check files
	if(!OpenFile(&F_Input, FileName001, &F_Output, argv[2]))
		return 1;

	//Save ZIP file
	SaveHeader(F_Output, Header);
	SaveStatistics(F_Output, Statistic);
	SaveEndBits(F_Output, F_Input);
	SaveOriginalSize(F_Output, &OriginalFileSize);
	SaveOriginalExtension(F_Output, OriginalExtension);
	SavePackFile(F_Input, F_Output);

	//Finish procedures
	fcloseall();
	FreeMemory(root);

	return 0;
}
int CheckParemeters(int CountParameters)
{
	int error = 0;
    if (CountParameters<2)
    {
        printf("Not enought parameters\n");
        printf("Usage: prog.exe inputfile\n");
		error = 1;
    }

	return !error;
}
int OpenFile(FILE** F_Input, char* FileNameInput, FILE** F_Output, char* FileNameOutput)
{
	int error = 0;
    *F_Input = fopen(FileNameInput, "rt");
	*F_Output = fopen(FileNameOutput, "wt");
    if(F_Input == 0 || F_Output == 0)
    {
        printf("File error!");
		error = 1;
    }
	return !error;
}
int ScanFile(FILE *F_Input, struct Unit Statistic[256])
{
	char buff;
	int FileSize = 0;
	int count;

	while (!feof(F_Input))
	{
		fread(&buff,sizeof(char),1,F_Input);
		Statistic[(unsigned char)buff].Count++;
		FileSize++;
	}

	if(FileSize > 0)
	{
		for(count=0;count<=255;count++)
			Statistic[count].freq = Statistic[count].Count / (float)FileSize;
	}
	return FileSize;
}
void ClearStat(struct Unit Statistic[256])
{
	for(int index = 0;index<=255;index++)
	{
		Statistic[index].Count=0;
		Statistic[index].Ch = index;
		Statistic[index].left = 0;
		Statistic[index].right = 0;

		for(int index2 = 0; index2 <= 255; index2++)
			*(Statistic[index].code + index2) = 0;
	}
}
void SortStatistic(struct Unit Statistic[256], struct Unit *Pointers[256])
{
	int HaveChange = 1;
	int index;

	while(HaveChange)
	{
		HaveChange = 0;
		for(index=0;index<=254;index++)
		{
			if(Statistic[index].Count < Statistic[index + 1].Count)
			{
				SwapElements(Statistic, index, index + 1);
				HaveChange = 1;
			}
		}
	}

	for(index=0;index<=255;index++)
	{
		Pointers[index] = &Statistic[index];
		//Mark elements in static memory
		Pointers[index]->Basic = 1;
	}

}
void SwapElements(struct Unit Statistic[256], int index1, int index2)
{
	int Ch = Statistic[index1].Ch;
	int Count = Statistic[index1].Count;
	float freq = Statistic[index1].freq;

	Statistic[index1].Ch = Statistic[index2].Ch;
	Statistic[index1].freq =Statistic[index2].freq;
	Statistic[index1].Count =Statistic[index2].Count;

	Statistic[index2].Ch = Ch;
	Statistic[index2].freq = freq;
	Statistic[index2].Count = Count;
}
struct Unit* BuildTree(struct Unit *Pointers[256], int index)
{
	struct Unit *New;

	New = (struct Unit*)malloc(sizeof(struct Unit));
	New->freq = Pointers[index-1]->freq + Pointers[index-2]->freq;
	New->left = Pointers[index-1];
	New->right = Pointers[index-2];
	New->code[0] = 0;
	//Mark elements in dinamic memory
	New->Basic = 0;

	if(index == 2)
		return New;

	InsertElement(Pointers, New, index);

	return BuildTree(Pointers, index - 1);

	return New;
}
void InsertElement(struct Unit *Pointers[256], struct Unit *Element, int index)
{

	while(Pointers[index]->freq < Element->freq && index > 0)
	{
		Pointers[index+1] = Pointers[index];
		index--;
	}

	Pointers[index+1] = Element;

}
void MakeCodes(struct Unit *root)
{
	if(root->left)
	{
		strcpy(root->left->code,root->code);
		strcat(root->left->code,"0");
		MakeCodes(root->left);
	}
	if(root->right)
	{
		strcpy(root->right->code,root->code);
		strcat(root->right->code,"1");
		MakeCodes(root->right);
}
}
void SaveFile001(FILE *F_Input, FILE* F_Output, struct Unit Statistic[256])
{
	unsigned char buff;

	while (!feof(F_Input))
	{
		fread(&buff,sizeof(char),1,F_Input);
		fputs(Statistic[buff].code, F_Output);
	}
}
unsigned char pack(unsigned char buf[])
{
	union CODE code;

	code.byte.b2 = buf[1] - '0';
	code.byte.b3 = buf[2] - '0';
	code.byte.b4 = buf[3] - '0';
	code.byte.b5 = buf[4] - '0';
	code.byte.b6 = buf[5] - '0';
	code.byte.b7 = buf[6] - '0';
	code.byte.b8 = buf[7] - '0';

	return code.ch;
}
int FileSize(FILE *File)
{
	int size;
	
	fseek(File, 0, SEEK_END);
	size = ftell(File);
	rewind(File);

	return size;
}

void SaveHeader(FILE* F_Output, char *Header)
{
	fwrite(Header, sizeof(char), 3, F_Output);
}
void SaveStatistics(FILE* F_Output, struct Unit Statistic[256])
{
	int index = 0;
	int count = 0;

	while(index<=255 && Statistic[index].Count)
		index++;

	count = index - 1;

	fwrite(&index, sizeof(int), 1, F_Output);

	for(index = 0; index <= count; index++)
	{
		fwrite(&Statistic[index].Ch, sizeof(char), 1, F_Output);
		fwrite(&Statistic[index].Count, sizeof(int), 1, F_Output);
	}
}

void SaveEndBits(FILE* F_Output, FILE* F_Input)
{
	int EndBits = FileSize(F_Input)%8;
	fwrite(&EndBits, sizeof(int), 1, F_Output);
}
void SaveOriginalSize(FILE* F_Output, int *Size)
{
	fwrite(Size, sizeof(int), 1, F_Output);
}
void SaveOriginalExtension(FILE* F_Output, char *Extension)
{
	fwrite(Extension, sizeof(char), 3, F_Output);
}
void SavePackFile(FILE *F_Input, FILE* F_Output)
{
	unsigned char buff[8];
	unsigned char byte;

	while (!feof(F_Input))
	{
		fread(&buff, sizeof(char), 8, F_Input);
		byte = pack(buff);
		fwrite(&byte, sizeof(unsigned char), 1, F_Output);
	}
}
void ReadOriginalExtension(char *OriginalExtension, char *ptr)
{
	int maxlen = 3;

	while(*ptr && *ptr != '.')
		ptr++;

	if(*ptr)
	{
		ptr++;
		while(*ptr && maxlen)
		{
			*OriginalExtension = *ptr;
			OriginalExtension++;
			ptr++;
			maxlen--;
		}
	}

}
void FreeMemory(struct Unit *Element)
{
	if(Element->left)
		FreeMemory(Element->left);
	if(Element->right)
		FreeMemory(Element->right);
	//Some elements is not in dinamic memory
	if(!Element->Basic)
		free(Element);
}