#include <stdio.h>
#define max 255

int MaxBefore(int);
void OutputSymbols(int);
void SetStat();
void OutputStat();
void ReadString();

char string[max];
int stat[255] = {0};

int main()
{
	ReadString();
	SetStat();
	OutputStat();

	return 0;
}

void ReadString()
{
	printf("Enter string: ");
	gets(string);
}

void SetStat()
{
	int Index = 0;
	while(string[Index])
	{
		stat[string[Index]]++;
		Index++;
	}
}
void OutputStat()
{
	int Counter = 99999;
	do{

		Counter = MaxBefore(Counter);
		if(Counter)
			OutputSymbols(Counter);

	}while(Counter);

}

int MaxBefore(int MaxValue)
{
	int Count=0,Index=' ',Value;
	
	for(;Index<=255;Index++)
	{
		Value = stat[Index];
		if(Value>Count && Value<MaxValue)
			Count = Value;
	}

	return Count;
}
void OutputSymbols(int Counter)
{
	int Index=' ',Value;

	printf("Count: %d - symbols(", Counter);

	for(;Index<=255;Index++)
	{
		Value = stat[Index];
		if(Value == Counter)
			printf("%c", Index);
	}
	printf(")\n");

}