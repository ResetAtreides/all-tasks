#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MaxRow 25
int Element(int Row, int Column);
void EnterData(int *Row, int *Column);
int main(int argc, char* argv[])
{
	int Row, Column;

	EnterData(&Row, &Column);

	printf("Value: %d \n", Element(Row, Column));

	return 0;
}
int Element(int Row, int Column)
{
	int Value = 0;
	if(Row == 1 || Column == 1 || Column == Row)
		Value = 1;
	else 
		Value = Element(Row - 1, Column - 1) + Element(Row - 1, Column);

	return Value;
}
void EnterData(int *Row, int *Column)
{
	char buff[255];
	printf("Enter Row:");
	fgets(buff,255,stdin);
	*Row = atoi(buff);

	printf("Enter Column:");
	fgets(buff,255,stdin);
	*Column = atoi(buff);

}