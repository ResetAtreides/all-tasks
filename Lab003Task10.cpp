#include <stdio.h>
#define max 255

void ReadString();
int ReadNumber();
void CutWord(int);
void SetRange(int *, int *, int);
void CutRange(int, int);
int ItSpace(char);

char string[max];

int main()
{
	ReadString();
	CutWord(ReadNumber());
	
	printf("New string: <%s>\n", string);

	return 0;
}

void ReadString()
{
	printf("Enter string: ");
	gets(string);
}
int ReadNumber()
{
	int WordNum;

	printf("Enter word number: ");
	scanf("%d", &WordNum);

	return WordNum;
}
void CutWord(int WordNum)
{
	int left=0,right=0;

	SetRange(&left,&right,WordNum);
	CutRange(left, right);
	
}
void SetRange(int *left, int *right, int WordNum)
{
	int count = 0;
	int index = 0;
	int word = 0;
	int Space;
	char sym;

	while(string[index])
	{
		sym = string[index];
		Space = ItSpace(sym);

		if((word == 0) && (Space == 0))	
		{
			count++;
			word = 1;
			if(count == WordNum)
				*left = index;
		}
		else if((word == 1) && Space)
		{
			word = 0;
			if(count == WordNum)
				*right = index;
		}

		index++;
	}

	if(count == WordNum)
		*right = index;
}

void CutRange(int left, int right)
{
	if(left == right)
	{
		printf("Word not found\n");
		return;
	}

	printf("Cut range: (%d-%d)\n", left, right);

	while(string[right])
	{
		string[left] = string[right];
		left++;
		right++;
	}

	string[left] = 0;
}
int ItSpace(char sym)
{
	int ErrCode = 0;

	if( sym == ' ' || sym == 13 || sym == ',' || sym == '.' || sym == '!')
		ErrCode = 1;

	return ErrCode;
}