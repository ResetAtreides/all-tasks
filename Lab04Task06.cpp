#include <stdio.h>
#define MaxSym 50
#define MaxKindred 10
struct Card
{
	char Name[MaxSym];
	int Age;
};
int KindredCount();
void EnterKindredData(struct Card Data[MaxKindred], int KindredCount, struct Card **young, struct Card **old);
void PrintResult(struct Card *young, struct Card *old);

int main()
{
	struct Card Data[MaxKindred];
	struct Card *young=0,*old=0;

	EnterKindredData(Data, KindredCount(), &young, &old);
	PrintResult(young, old);

	return 0;
}

int KindredCount()
{
	int KindredCount = 0;
	while(KindredCount == 0 || KindredCount > MaxKindred)
	{
		fflush(stdin);
		printf("How much kindred do you have? :");
		scanf("%d", &KindredCount);
	}
	return KindredCount;
}
void EnterKindredData(struct Card Data[MaxKindred], int KindredCount, struct Card **young, struct Card **old)
{
	unsigned int index;
	for(index=0;index < KindredCount; index++)
	{
		printf("Enter name & age kindred N%d :", index+1);
		scanf("%s %d", &Data[index].Name, &Data[index].Age);

		if(*young == 0)
		{
			*young = &Data[index];
			*old = &Data[index];
		}
		else if(Data[index].Age < (*young)->Age)
			*young = &Data[index];

		else if(Data[index].Age > (*old)->Age)
			*old = &Data[index];
	}
}
void PrintResult(struct Card *young, struct Card *old)
{
	printf("Young: %s(%d)\nOld: %s(%d)\n", young->Name, young->Age, old->Name, old->Age);
}