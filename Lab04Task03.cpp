#include <stdio.h>
#define max 255
void DataInput(char* string);
char *EndOfString(char* Pointer);
int TestPolindrome(char* PointerBegin, char* PointerEnd);
void ShowResult(int Polindrome);

int main()
{
	char string[max];
	char *PointerBegin = string;
	char *PointerEnd;

	DataInput(string);
	PointerEnd = EndOfString(PointerBegin);
	ShowResult(TestPolindrome(PointerBegin, PointerEnd));
}

void DataInput(char *string)
{
	printf("Enter string :");
	gets(string);
}

char* EndOfString(char *PointerBegin)
{
	char *EndPointer = PointerBegin;

	while(*EndPointer)
		EndPointer++;
	
	return EndPointer-1;
}


int TestPolindrome(char* PointerBegin, char* PointerEnd)
{

	while(PointerBegin<PointerEnd)
	{
		if(*PointerBegin != *PointerEnd)
			return 0;
		PointerBegin++;
		PointerEnd--;
	}

	return 1;
}
void ShowResult(int Polindrome)
{
	printf("It's %s palindrome \n", (Polindrome?"":"not"));
}
