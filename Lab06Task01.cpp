#include <stdio.h>
#include <stdlib.h>
#define Size 30
void PrintFractale(char (*arr)[Size+1], int x, int y, int scale);
void OutputFractale(char (*arr)[Size+1]);
void ClearData(char (*arr)[Size+1]);
int main()
{

	char arr[Size][Size+1];
	int x = Size/2,y = Size/2;
	int scale = 27;

	ClearData(arr);
	PrintFractale(arr,x,y,scale);
	OutputFractale(arr);

	return 0;
}
void PrintFractale(char (*arr)[Size+1], int x, int y, int scale)
{
	if(scale > 1)
	{
		scale = scale / 3;
		PrintFractale(arr, x, y, scale);
		PrintFractale(arr, x - scale, y, scale);
		PrintFractale(arr, x + scale, y, scale);
		PrintFractale(arr, x, y - scale, scale);
		PrintFractale(arr, x, y + scale, scale);
	}
	else
	{
		arr[y][x] = '*';
	}


}
void ClearData(char (*arr)[Size+1])
{
	int x,y;
	for(y=0;y<Size;y++)
		for(x=0;x<=Size;x++)
			arr[y][x] = ' ';
}
void OutputFractale(char (*arr)[Size+1])
{
	int y;

	for(y=0;y<Size;y++)
		arr[y][Size] = 0;

	system("cls");

	for(y=0;y<Size;y++)
		printf("%s\n", arr[y]);
}