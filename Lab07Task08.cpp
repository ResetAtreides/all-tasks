#include<stdio.h>
#include<stdlib.h>
int CheckParemeters(int CountParameters);
int eval(char *buf);
char partition(char *buf, char *expr1, char *expr2);
int ItsOperator(char symb);
int ItsDigit(char symb);
int main(int argc, char* argv[])
{
	if(CheckParemeters(argc))
		printf("%s = %d\n", argv[1], eval(argv[1]));

	return 0;
}
int eval(char *buf)
{
	char expr1[255];
	char expr2[255];
	int summ;
	char oper;

	oper = partition(buf, expr1, expr2);
	if(oper == 0)
		summ = atoi(expr1);
	else
	{
		if(oper == '+')
			summ = eval(expr1) + eval(expr2);
		else if(oper == '-')
			summ = eval(expr1) - eval(expr2);
		else if(oper == '*')
			summ = eval(expr1) * eval(expr2);
		else if(oper == '/')
			summ = eval(expr1) / eval(expr2);
	}

	return summ;
}
 char partition(char *buf, char *expr1, char *expr2)
{
	char *ptr = buf;
	int index;
	char sym;
	int flag;
	char oper;
	int open;

	if(*ptr == '(')
	{
		open = 1;
		ptr++;
		flag = 0;
		index = 0;

		while(*ptr)
		{
			sym = *ptr;

			if(sym == '(')
				open++;
			else if(sym == ')')
				open--;

			if(ItsOperator(sym) && open == 1)
			{
				flag = 1;
				oper = sym;
				*(expr1  + index) = 0;
				index = 0;
			}
			else
			{
				if(!flag)
					*(expr1  + index) = sym;
				else
					*(expr2  + index) = sym;

				index++;
					
			}
			ptr++;
		}
		
		if(!flag)
			oper = 0;
		else
			*(expr2+index-1) = 0;

	}
	else
	{
		index = 0;
		flag = 0;
		while(*ptr)
		{
			sym = *ptr;
			if(ItsOperator(sym) && !flag)
			{
				flag = 1;
				oper = sym;
				*(expr1+index) = 0;
				index = 0;
			}
			else
			{
				if(flag)
					*(expr2+index) = sym;
				else
					*(expr1+index) = sym;
				index++;
			}
			ptr++;
		}
		
		if(!flag)
			oper = 0;
		else
			*(expr2+index) = 0;
	}
	
	return oper;

}
int CheckParemeters(int CountParameters)
{
	int error = 0;
    if (CountParameters<2)
    {
        printf("Not enought parameters\n");
        printf("Usage: prog.exe expression\n");
		error = 1;
    }
	return !error;
}
int ItsOperator(char symb)
{
	int Operator = 0;
	if(symb == '+' || symb == '-' || symb == '/' || symb == '*')
		Operator = 1;
	return Operator;
}
int ItsDigit(char symb)
{
	int Digit = 0;
	if(symb >='0' && symb <= '9')
		Digit = 1;
	return Digit;
}