#include <stdio.h>
#include <string.h>
#define maxLines 10
#define maxSymbols 50
void DataInput(char (*)[maxSymbols], char **words);
void DataSort(char **words);
void SetLenght(char **pointers, int *lenght);
void SortPointers(char **pointers, int *lenght);
void SwapPointers(char **pointers, int *lenght, int FirstIndex);
void DataOutput(char **pointers);

int main()
{
	char data[maxLines][maxSymbols];
	char *pointers[maxLines]={0};

	DataInput(data, pointers);
	DataSort(pointers);
	DataOutput(pointers);

	return 0;
}

void DataInput(char (*ptr)[maxSymbols], char **pointers)
{
	int index=0;

	do{
		index++;
		printf("Enter string %02d :", index);
		gets(ptr[index-1]);
		if(ptr[index-1][0])
			pointers[index-1] = ptr[index-1];

	}while(index<maxLines && ptr[index-1][0]);

	return;
}

void DataSort(char **pointers)
{
	int lenght[maxLines]={0};

	SetLenght(pointers, lenght);
	SortPointers(pointers, lenght);
}

void SetLenght(char **pointers, int *lenght)
{
	int index=0;

	while(pointers[index])
	{
		lenght[index] = strlen(pointers[index]);
		index++;
	}
}

void SortPointers(char **pointers, int *lenght)
{
	int flag=1;
	int index;

	while(flag)
	{
		flag=0;
		index = 0;
		while(lenght[index+1])
		{
			if(lenght[index]>lenght[index+1])
			{
				SwapPointers(pointers, lenght, index);
				flag=1;
			}
			index++;
		}
	}
}

void SwapPointers(char **pointers, int *lenght, int FirstIndex)
{
	int TempValue;
	char *TempPointer;

	TempValue = lenght[FirstIndex];
	lenght[FirstIndex] = lenght[FirstIndex+1];
	lenght[FirstIndex+1] = TempValue;

	TempPointer = pointers[FirstIndex];
	pointers[FirstIndex] = pointers[FirstIndex+1];
	pointers[FirstIndex+1] = TempPointer;

}

void DataOutput(char **pointers)
{
	int index=0;

	printf("Sorting:\n");

	while(pointers[index])
	{
		printf("String %2d: %s\n", index+1, pointers[index]);
		index++;
	}
}