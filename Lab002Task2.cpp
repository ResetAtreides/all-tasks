#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int value1,value2,count=1;

	srand(time(NULL));
	value1 = rand()%100 + 1;

	do{
		printf("Enter you value(1..100):");
		fflush(stdin);
		scanf("%d", &value2);

		if(value2>value1)
			printf("You value too big\n");
		else if(value2<value1)
			printf("You value too small\n");
		else
			printf("You are right! You count: %d\n", count);

		count++;

	}while(value1 != value2);

	return 0;
}