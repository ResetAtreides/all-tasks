#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MaxStrings 10
#define MaxChar 50
struct StringStruct
{
	char text[MaxChar];
	int len;
};
void CheckParemeters(int CountParameters);
void OpenFiles(FILE** F_Input, char* FileNameInput, FILE** F_Output, char* FileNameOutput);
int ReadData(FILE* F_Input, struct StringStruct StringsData[MaxStrings]);
void SortData(struct StringStruct StringsData[MaxStrings], int CountString);
void ExchangeString(struct StringStruct *StringsDest, struct StringStruct *StringsSource, int *flag);
void SaveData(FILE* F_Output, struct StringStruct StringsData[MaxStrings], int CountString);

int main(int argc, char* argv[])
{
	struct StringStruct StringsData[MaxStrings];
	FILE *F_Input=NULL, *F_Output=NULL;
	int CountString;

	CheckParemeters(argc);
	OpenFiles(&F_Input, argv[1], &F_Output, argv[2]);
	CountString = ReadData(F_Input, StringsData);
	SortData(StringsData, CountString);
	SaveData(F_Output, StringsData, CountString);

	fcloseall();
	return 0;
}
void CheckParemeters(int CountParameters)
{
	if (CountParameters<3)
	{
		printf("Not enought parameters\n");
		printf("Usage: prog.exe FileInput FileOutput\n");
		exit(1);
	}
}
void OpenFiles(FILE** F_Input, char* FileNameInput, FILE** F_Output, char* FileNameOutput)
{
	*F_Input = fopen(FileNameInput, "rt");
	*F_Output = fopen(FileNameOutput, "wt");
	if(F_Input == 0 || F_Output == 0)
	{
		printf("File error!");
		exit (2);
	}
}
int ReadData(FILE* F_Input, struct StringStruct StringsData[MaxStrings])
{
	int index=0;
	int count=0;
	char* pointer;
	int lenght;

	while(index<MaxStrings)
	{
		pointer = fgets(StringsData[index].text, MaxChar, F_Input);

		if(pointer == 0)
			return index;
		else
		{
			lenght = strlen(pointer);
			if(StringsData[index].text[lenght-1] = '\n')
			{
				StringsData[index].text[lenght-1] = 0;
				lenght=lenght-1;
			}
			StringsData[index].len = lenght;
			index++;
		}

	}
	return index;
}



void SortData(struct StringStruct StringsData[MaxStrings], int CountString)
{
	int index=0;
	int flag=1;

	while(flag)
	{
		flag = 0;

		for(index=1;index<CountString;index++)
		{
			if(StringsData[index-1].len > StringsData[index].len)
			{
				ExchangeString(&StringsData[index-1], &StringsData[index], &flag);
			}
		}
	}
}
void ExchangeString(struct StringStruct *StringsDest, struct StringStruct *StringsSource, int *flag)
{
	char TempBuff[MaxChar];
	int TempLen;

	strcpy(TempBuff, StringsDest->text);
	strcpy(StringsDest->text, StringsSource->text);
	strcpy(StringsSource->text, TempBuff);

	TempLen = StringsDest->len;
	StringsDest->len = StringsSource->len;
	StringsSource->len = TempLen;

	*flag = 1;
}
void SaveData(FILE* F_Output, struct StringStruct StringsData[MaxStrings], int CountString)
{
	int index;
	for(index=0;index<CountString;index++)
	{
		fprintf(F_Output, "%s", StringsData[index].text);
		fputc('\n', F_Output);
	}
}
