#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define max 50
int SetSize();
int SizeFromPow(int pow);
void FillArray(int *arr, int size);
void CheckSummStandart(int *arr, int size);
void CheckSummRecurse(int *arr, int size);
long long Solve(int *ptr, int count);
int main()
{
	int size;
	int *arr;
	
	srand(time(0));

	size = SetSize();
	arr = (int *)malloc(size * sizeof(int));
	FillArray(arr, size);

	CheckSummStandart(arr, size);
	//Or too fast(less 2 sec), or not enought memory for recurse
	CheckSummRecurse(arr, size);

	free(arr);

}
int SetSize()
{
	char buff[max];
	printf("Enter power of 2 for massive size: ");
	fgets(buff, max, stdin);
	return SizeFromPow(atoi(buff));

}
int SizeFromPow(int pow)
{
	int value = 1;
	int index;
	
	for(index=0;index<pow;index++)
		value*=2;

	return value;

}
void FillArray(int *arr, int size)
{
	for(int index=0;index<size;index++)
		arr[index] = rand();
}
void CheckSummStandart(int *arr, int size)
{
	long long summ = 0;

	time_t StartTime = clock();
	
	while(size--)
		summ += arr[size];

	printf("Standart summ: %d, time:%d msec.\n", summ, clock() - StartTime);

}
void CheckSummRecurse(int *arr, int size)
{
	long long summ = 0;
	time_t StartTime = clock();

	summ = Solve(arr, size);

	printf("Recurse summ: %d, time:%d msec.\n", summ, clock() - StartTime);

}
long long Solve(int *ptr, int count)
{
	long long summ;

	if(count>1)
	{
		return Solve(ptr + 1, count - 1) + (long long)(*ptr);
	}
	else
		return (long long)(*ptr);
}