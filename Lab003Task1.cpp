#include <stdio.h>
#define max 255

int ItSpace(char);

char string[max];


int main()
{
	int flag=0,index=0,counter=0;

	printf("Enter the string: ");
	gets(string);
	
	while(index<max && string[index])
	{
		if( flag == 0 && ItSpace(string[index]) == 0 )
			flag = 1;
		else if(flag && ItSpace(string[index]))
		{
			flag = 0;
			counter++;
		};

		index++;
	};

	if(flag)
		counter++;

	printf("Words: %d\n", counter);

	return 0;
}

int ItSpace(char sym)
{
	int ErrCode = 0;

	if( sym == ' ' || sym == 13 )
		ErrCode = 1;

	return ErrCode;
}