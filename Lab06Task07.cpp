#include <stdio.h>
#define high 9
#define lengt 27
int Step(char (*arr)[lengt+1], int y, int x);
void PrintWay(char arr[high][lengt+1]);
int main()
{
	char arr[high][lengt+1] =  {"###########################",
								"#        #    #           #",
								"#######  #    #           #",
								"#        #    #########  ##",
								"# #####  #                #",
								"#     #  #    #########   #",
								"##### #  #    #           #",
								"      #       #         ###",
								"###########################"};

	int y = 4, x = 10;

	if(Step(arr, y, x))
		PrintWay(arr);
	else
		printf("No way");

	return 0;
}

int Step(char arr[high][lengt+1], int y, int x)
{
	char block;

	block = arr[y][x];
	if(block == '#' || block == 'X')
		return 0;
	else if(y==0 || x==0 || y==(high-1) || y==(lengt-1))
	{
		return 1;
	}
	else
	{
		arr[y][x] = 'X';

		if(Step(arr, y-1, x))
		{
			arr[y-1][x] = '.';
			return 1;
		}

		if(Step(arr, y+1, x))
		{
			arr[y+1][x] = '.';
			return 1;
		}

		if(Step(arr, y, x-1))
		{
			arr[y][x-1] = '.';
			return 1;
		}

		if(Step(arr, y, x+1))
		{
			arr[y][x+1] = '.';
			return 1;
		}

		arr[y][x] = ' ';

		return 0;
	}
}
void PrintWay(char arr[high][lengt+1])
{
	int y=0;
	for(y=0;y<high;y++)
		printf("%s\n", arr[y]);
}