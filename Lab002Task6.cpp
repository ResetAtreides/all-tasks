#include <stdio.h>
#include <string.h>

#define MAX 255

void ShiftLeft(int);
void MoveArea(int, int);
void DeleteDoubleSpaces();
void DeleteLastSpace();

int FirstSym(int);
char string[MAX];

int main()
{
	printf("Enter a string:");
	fgets(string,255,stdin);
	string[strlen(string)-1] = 0;

	ShiftLeft(0);
	DeleteDoubleSpaces();
	DeleteLastSpace();

	printf("New string:\n<%s>\n", string);

	return 0;
}

void ShiftLeft(int begin)
{
	int end = FirstSym(begin);
	if(begin != end)
		MoveArea(begin, end);
}
void DeleteDoubleSpaces()
{
	int index=0;
	while(string[index])
	{
		if(string[index] == ' ' && string[index+1] == ' ')
			ShiftLeft(index+1);

		index++;
	}
}

void DeleteLastSpace()
{
	//The space character may be only one before '\n', because
	// DeleteDoubleSpaces() move '\n' to the left
	if(string[strlen(string)-1] = ' ')
		string[strlen(string)-1] = 0;
}

int FirstSym(int begin)
{
	if(string[begin] != ' ')
		return begin;

	int index = begin+1;
	
	while(string[index] == ' ')
	index++;

	return index;
}
void MoveArea(int begin, int end)
{
	int index = 0;

	do{
		string[begin+index] = string[end+index];
		index++;
	}while(string[begin+index-1]);
}
