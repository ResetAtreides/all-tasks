#include <stdio.h>
#include <string.h>

int modificate();

char string[255];


int is_num(char code)
{
	return (code >= '0' && code <= '9');
}

int main()
{

	printf("Enter a string:");
	fgets(string,255,stdin);

	while(modificate());
	
	printf("Sorted string: \n%s\n", string);

	return 0;
}

int modificate()
{
	int flag =0,index = 0;
	char symb1,symb2;

	while(string[index+1] != 0)
	{
		symb1 = string[index];
		symb2 = string[index+1];

		if(is_num(symb2) && (is_num(symb1) == 0))
		{
			string[index] = symb2;
			string[index+1] = symb1;
			flag = 1;
		}

		index++;
	}

	return flag;
}
