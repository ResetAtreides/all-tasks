#include <stdio.h>

int main()
{
	char sign;
	float value,result;
	int exitcode=0;

	printf("Enter value: (d - degree, r - radian)");
	fflush(stdin);

	scanf("%f%c", &value, &sign);

	if(sign == 'd')
	{
		result = value/180*3.1415;
		printf("%f degrees = %f radians\n", value, result);
	}
	else if(sign == 'r')
	{
		result = value/3.1415*180;
		printf("%f radians = %f degrees\n", value, result);
	}
	else
	{
		printf("Wrong symbol\n");
		exitcode = 1;
	}

	return exitcode;
}
