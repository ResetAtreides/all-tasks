#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define MaxWord 255
int CheckParemeters(int CountParameters);
int OpenFiles(FILE** F_Input, char* FileNameInput, FILE** F_Output, char* FileNameOutput);
int ItChar(char *pointer);
void MeshWord(char *Word, int Index);
void OutputData(FILE *F_Input, FILE *F_Output);
int main(int argc, char* argv[])
{
    FILE *F_Input=NULL, *F_Output=NULL;

	srand(time(0));
    if(CheckParemeters(argc) && OpenFiles(&F_Input, argv[1], &F_Output, argv[2]))
	{
		OutputData(F_Input, F_Output);
		fcloseall();
	}

	return 0;
}
void OutputData(FILE *F_Input, FILE *F_Output)
{

	char *ReadPointer;
	int InWord = 0;
	int Index = 0;
	char Word[MaxWord];
	char Symb[2];

	srand(time(0));

	ReadPointer = fgets(Symb,2,F_Input);
	while(ReadPointer)
	{
		if(!InWord && !ItChar(Symb))
			fprintf(F_Output, "%s", Symb);
		else if(!InWord && ItChar(Symb))
		{
			InWord = 1;
			Index = 0;
			Word[Index] = Symb[0];
		}
		else if(InWord && ItChar(Symb))
		{
			Index++;
			Word[Index] = Symb[0];
		}
		else if(InWord && !ItChar(Symb))
		{
			Word[Index + 1] = Symb[0];
			Word[Index + 2] = 0;
			MeshWord(Word, Index);
			fprintf(F_Output, "%s", Word);
			InWord = 0;
		}

		ReadPointer = fgets(Symb,2,F_Input);
	}

}
int CheckParemeters(int CountParameters)
{
	int error = 0;
    if (CountParameters<3)
    {
        printf("Not enought parameters\n");
        printf("Usage: prog.exe FileInput FileOutput\n");
		error = 1;
    }
	return !error;
}
int OpenFiles(FILE** F_Input, char* FileNameInput, FILE** F_Output, char* FileNameOutput)
{
	int error = 0;
    *F_Input = fopen(FileNameInput, "rt");
    *F_Output = fopen(FileNameOutput, "wt");

    if(F_Input == 0 || F_Output == 0)
    {
        printf("File error!");
		error = 1;
    }

	return !error;
}
int ItChar(char *pointer)
{
	int ItChar;
	//possible problem with code page//
	if((*pointer >= 'A' && *pointer <= 'Z') ||
		(*pointer >= 'a' && *pointer <= 'z') ||
		//russian letters{
		(*pointer >= '�' && *pointer <= '�') ||
		(*pointer >= '�' && *pointer <= '�'))
		//russian letters}
		ItChar = 1;
	else
		ItChar = 0;

	return ItChar;
}
void MeshWord(char *Word, int Index)
{
	int Count;
	int Source,Dest;
	char Buff;

	if(Index > 3)
	{
		Count = rand()%Index + 1;
		while(Count > 0)
		{
			Source = rand()%(Index-1) + 1;
			Dest = rand()%(Index-1) + 1;
			Buff = Word[Source];
			Word[Source] = Word[Dest];
			Word[Dest] = Buff;
			Count--;
		}
	}
}

