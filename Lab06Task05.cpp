#include <stdio.h>
#include <time.h>
#define MaxRow 25
int Element(int Row, int Column);
int CheckParemeters(int CountParameters);
int OpenFiles(FILE** F_Output, char* FileNameOutput);
int main(int argc, char* argv[])
{
	int Row, Column;
	FILE *F_Output=NULL;

	if(CheckParemeters(argc) && OpenFiles(&F_Output, argv[1]))
	{

		for(Row=1;Row<=MaxRow;Row++)
		{
			time_t StartTime = clock();
			printf("Row: %2d  <", Row);
			fprintf(F_Output, "%d", Row);
			for(Column = 1; Column <= Row; Column++)
			{
				printf("%d ", Element(Row, Column));
			}
			time_t StartEnd = clock();
			printf(">(Time: %d)\n", StartEnd-StartTime);
			fprintf(F_Output, " %d\n", StartEnd-StartTime);
		}

	}

	return 0;
}
int Element(int Row, int Column)
{
	int Value = 0;
	if(Row == 1 || Column == 1 || Column == Row)
		Value = 1;
	else 
		Value = Element(Row - 1, Column - 1) + Element(Row - 1, Column);

	return Value;
}
int CheckParemeters(int CountParameters)
{
	int error = 0;
    if (CountParameters<2)
    {
        printf("Not enought parameters\n");
        printf("Usage: prog.exe FileOutput\n");
		error = 1;
    }
	return !error;
}
int OpenFiles(FILE** F_Output, char* FileNameOutput)
{
	int error = 0;
    *F_Output = fopen(FileNameOutput, "wt");

    if(F_Output == 0)
    {
        printf("File error!");
		error = 1;
    }

	return !error;
}
