#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define MaxPassword 10
#define SymInPassword 8

char symbol();
void makepassword(int);
char lett(int);
char num();

int main()
{
	int counter = MaxPassword;
	srand(time(NULL));

	while(counter--)
		makepassword(MaxPassword - counter);

	return 0;
}

void makepassword(int counter)
{
	char pass[SymInPassword + 1];
	int sym = SymInPassword;

	while(sym--)
		pass[sym] = symbol();

	pass[SymInPassword] = 0;

	printf("Password %03d: %s\n", counter, pass);
}

char symbol()
{
	int type = rand()%3;
	char sym;

	if(type == 2)
		sym = num();
	else
		sym = lett(type);

	return sym;
}


char lett(int cups)
{
	int range = ('z' - 'a');
	int base = (cups?'a':'A');
	char sym = rand()%range + base;

	return sym;
}

char num()
{
	int range = ('9' - '0');
	int base = '0';
	char sym = rand()%range + base;

	return sym;
}