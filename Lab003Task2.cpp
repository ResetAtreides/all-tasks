#include <stdio.h>
#define max 255

int ItSpace(char);

char string[max];


int main()
{
	int flag=0,index=0,counter=0,len=0;
	char sym;

	printf("Enter the string: ");
	gets(string);
	
	while(index<max && string[index])
	{

		sym = string[index];

		if(flag == 0 && ItSpace(sym) == 0)
		{
			flag = 1;
			len = 1;
			counter++;

			printf("Word %d : %c", counter, sym);
		}
		else if(flag && ItSpace(sym))
		{
			flag = 0;
			printf(" - %d symbols\n", len);
			
		}
		else if(flag && ItSpace(sym) == 0)
		{
			len++;
			putchar(sym);
		};

		index++;
	};

	if(flag)
	{
		printf(" - %d symbols\n", len);
	};

	if(counter == 0)
	{
		printf("No words\n");
	}

	return 0;
}

int ItSpace(char sym)
{
	int ErrCode = 0;

	if( sym == ' ' || sym == 13 )
		ErrCode = 1;

	return ErrCode;
}