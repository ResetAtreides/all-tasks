#include <stdio.h>
#define max 255

void ReadString();
int ReadNumber();
void OutputWord(int);
int ItSpace(char);

char string[max];

int main()
{
	ReadString();
	OutputWord(ReadNumber());

	return 0;
}

void ReadString()
{
	printf("Enter string: ");
	gets(string);
}
int ReadNumber()
{
	int WordNum;

	printf("Enter word number: ");
	scanf("%d", &WordNum);

	return WordNum;
}
void OutputWord(int WordNum)
{
	int count = 0;
	int index = 0;
	int word = 0;
	int Space;
	char sym;

	printf("Word number: %d - <", WordNum);

	while(string[index])
	{
		sym = string[index];
		Space = ItSpace(sym);

		if((word == 0) && (Space == 0))	
		{
			count++;
			word = 1;
		}
		else if((word == 1) && Space)
		{
			word = 0;
		}

		if(word && (count == WordNum) && Space == 0)
			printf("%c", sym);

		index++;
	}

	printf(">\n");
}
int ItSpace(char sym)
{
	int ErrCode = 0;

	if( sym == ' ' || sym == 13 || sym == ',' || sym == '.' || sym == '!')
		ErrCode = 1;

	return ErrCode;
}