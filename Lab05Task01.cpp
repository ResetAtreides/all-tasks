#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MaxBuff 255
#define MaxWords 25
struct WordsStruct{
	char *WordsBegin;
	char *WordsEnd;
	int Number;
};
void EnterString(char *buff);
int getWords(char *buff, struct WordsStruct *Data);
int itDevider(char symb);
void RandomSort(struct WordsStruct *Data, int WordsCount);
void SetNumber(struct WordsStruct *Data, int WordsCount, int Count);
void SetRandom();
void PrintSorted(struct WordsStruct *Data, int WordsCount);
void PrintWord(struct WordsStruct *Data, int index);
int FindFromIndex(struct WordsStruct *Data, int WordsCount, int index);
int main()
{
	char buff[MaxBuff];
	struct WordsStruct Words[MaxWords]={0};
	int WordsCount;
	
	SetRandom();
	EnterString(buff);
	WordsCount = getWords(buff, Words);
	RandomSort(Words, WordsCount);
	PrintSorted(Words, WordsCount);

	return 0;
}
void SetRandom()
{
	srand(time(0));
}
void EnterString(char *buff)
{
	printf("Enter the string: ");
	gets(buff);
}
int getWords(char *buff, struct WordsStruct Data[MaxWords])
{
	int WordsCount = 0;
	int inWord = 0;
	char *ptr,*ptrBegin;
	char symb;
	
	ptr=ptrBegin=buff;

	while(*ptr && *ptr!=13)
	{
		symb = *ptr;
		if(!inWord && !itDevider(symb))
		{
			inWord = 1;
			ptrBegin = ptr;
		}
		else if(inWord && itDevider(symb))
		{
			inWord=0;
			Data[WordsCount].WordsBegin = ptrBegin;
			Data[WordsCount].WordsEnd = ptr-1;
			WordsCount++;
		}
		ptr++;
	}
	if(inWord)
	{
		Data[WordsCount].WordsBegin = ptrBegin;
		Data[WordsCount].WordsEnd = ptr-1;
		WordsCount++;
	}

	return WordsCount;
}
int itDevider(char symb)
{
	return symb == ' ' || symb == 0 || symb == 13;
}
void RandomSort(struct WordsStruct Data[MaxWords], int WordsCount)
{
	int Count;
	
	for(Count = 1; Count<=WordsCount; Count++)
		SetNumber(Data, WordsCount, Count);
}
void SetNumber(struct WordsStruct Data[MaxWords], int WordsCount, int Count)
{
	int SetFlag = 1;
	int index = 0;

	index = rand()%WordsCount;
	while(SetFlag)
	{
		if(Data[index].Number == 0)
		{
			Data[index].Number = Count;
			SetFlag = 0;
		}
		else
		{
			index++;
			if(index == WordsCount)
				index = 0;
		}
	}
}
void PrintSorted(struct WordsStruct Data[MaxWords], int WordsCount)
{
	int index;
	int WordWithIndex;

	printf("\n");
	for(index=1;index<=WordsCount;index++)
	{
		WordWithIndex = FindFromIndex(Data, WordsCount, index);
		PrintWord(Data, WordWithIndex);
		printf(" ");
	}
	printf("\n");
}
void PrintWord(struct WordsStruct Data[MaxWords], int index)
{
	char *ptr;

	for(ptr=Data[index].WordsBegin; ptr<=Data[index].WordsEnd; ptr++)
	{
		printf("%c", *ptr);
	}
}
int FindFromIndex(struct WordsStruct Data[MaxWords], int WordsCount, int index)
{
	int Count;
	for(Count=0; Count<WordsCount; Count++)
	{
		if(Data[Count].Number == index)
			return Count;
	}
	return 0;
}