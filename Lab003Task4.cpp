#include <stdio.h>
#include <stdlib.h>

#define max 255
#define maxdigit 3

int ItDigit(char);
void EndNum(char *, int * , int);

int main()
{
	char string[max];
	char num[maxdigit + 1];
	char sym;
	int summ=0,StringIndex=0,DigitIndex=0;
	int flag = 0;

	printf("Enter the string: ");
	gets(string);

	while(string[StringIndex])
	{
		sym = string[StringIndex];

		if((flag == 0) && ItDigit(sym))
		{
			flag=1;
			DigitIndex=0;
			num[DigitIndex] = sym;
		}
		else if((flag) && ItDigit(sym))
		{
			if(DigitIndex<maxdigit-1)
			{
				DigitIndex++;
				num[DigitIndex] = sym;
			}
			else
			{
				EndNum(num, &summ, DigitIndex);
				DigitIndex = 0;
				num[DigitIndex] = sym;
			}

		}
		else if((flag) && (ItDigit(sym) == 0))
		{
			EndNum(num, &summ, DigitIndex);
			flag = 0;
		}
		
		StringIndex++;
	}

	if(flag)
		EndNum(num, &summ, DigitIndex);

	printf(" = %d\n", summ);


	return 0;
}

int ItDigit(char sym)
{
	int code = ((sym>='0') && (sym<='9')?1:0);
	return code;
}

void EndNum(char *num, int *summ, int DigitIndex)
{
	num[DigitIndex+1] = 0;

	int digit = atoi(num);

	if(*summ !=0)
		putchar('+');
	printf("%d", digit);

	*summ = *summ + digit;
}