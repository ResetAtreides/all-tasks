#include <stdio.h>

int main()
{
	int hour,min,sec = 0;

	printf("What time it is? (HH:MM:SS)");

	fflush(stdin);
	scanf("%d:%d:%d", &hour, &min, &sec);

	if(hour >= 6 && hour < 11)
		printf("Good morning!\n");
	else if(hour >= 11 && hour < 17)
		printf("Have a nice day!\n");
	else if(hour >= 17 && hour < 23)
		printf("Good evening!\n");
	else
		printf("Good night!\n");

	return 0;
}