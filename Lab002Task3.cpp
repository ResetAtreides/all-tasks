#include <stdio.h>

int main()
{
	int raw,col,n,stars,spaces;

	printf("Enter N:");
	scanf("%d",&n);

	for(raw=1; raw < n+1; raw++)
	{
		stars = 2*raw-1;
		spaces = n - raw;

		for(col=1; col < spaces+stars+1; col++)
		{
			if(col <= spaces)
				putchar(' ');
			else
				putchar('*');
		}
		putchar('\n');

	}

	return 0;
}