#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define max 10
#define randMax 10

int rnd();
void FillArray();

int arr[max];

int main()
{
	int i;
	int Value, MinValue = 0,MaxValue = 0;

	FillArray();

	for(i=0;i<max;i++)
	{
		Value = arr[i];
		if(Value < MinValue)
			MinValue = Value;
		if(Value > MaxValue)
			MaxValue = Value;
	};

	printf("\n%d + %d = %d\n", MinValue, MaxValue, (MinValue+MaxValue));

	return 0;
}

void FillArray()
{
	int index;

	srand(time(NULL));
	for(index=0; index < max; index++)
	{
		arr[index] = rnd();
		printf("%3d ", arr[index]);
	}

}

int rnd()
{
	return rand()%(randMax*2) - randMax;
}
