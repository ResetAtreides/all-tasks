#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define max 6
#define randMax 10

int rnd();
void FillArray();
int SummRange(int, int);

int arr[max];

int main()
{
	int i;
	int iNeg = -1,iPos = -1;
	int summ=0;

	FillArray();

	for(i=0;i<max;i++)
	{
		if((iNeg == -1) && (arr[i] < 0))
			iNeg = i;
		if((iPos == -1) && (arr[max-i-1] > 0))
			iPos = max-i-1;
	};

	summ = SummRange(iNeg, iPos);

	printf(" = %d\n", summ);

	return 0;
}

void FillArray()
{
	int index;

	srand(time(NULL));
	for(index=0; index < max; index++)
	{
		arr[index] = rnd();
		printf("%3d ", arr[index]);
	}

}

int rnd()
{
	return rand()%randMax-(randMax/2);
}

int SummRange(int iNeg, int iPos)
{
	int summ=0;
	int i,begin,end;
	int val;

	if((iNeg == -1) || (iPos == -1))
	{
		printf("\nNo posible range\n");
		return 0;
	}
	else
		printf("\nPositive index: %d\nNegative index: %d\nSumm = ", iPos+1,iNeg+1);

	if(iNeg > iPos)
	{
		begin = iPos;
		end = iNeg;
	}
	else
	{
		begin = iNeg;
		end = iPos;
	}

	for(i=begin;i<=end;i++)
	{
		val = arr[i];
		printf("%c%d", (val<0?' ':'+'), val);
		summ += val;
	}

	return summ;
}