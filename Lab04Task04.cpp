#include <stdio.h>
#define max 255

int ItSpace(char);
void EnterString(char *string);
void PrintResult(int maxlen, char *LongBegin, char *LongEnd);
int AnalizeString(char *string, char **LongBegin, char **LongEnd);

int main()
{
	char string[max];
	char *LongBegin, *LongEnd;
	int maxlen;

	EnterString(string);
	maxlen = AnalizeString(string, &LongBegin, &LongEnd);
	PrintResult(maxlen, LongBegin, LongEnd);

	return 0;
}

int ItSpace(char sym)
{
	int ErrCode = 0;

	if( sym == ' ' || sym == 13 )
		ErrCode = 1;

	return ErrCode;
}
void EnterString(char *string)
{
	printf("Enter the string: ");
	gets(string);
}
void PrintResult(int maxlen, char *LongBegin, char *LongEnd)
{
	char *ptr;

	if(maxlen)
	{
		printf("Max lenght: %d (", maxlen);
		for(ptr=LongBegin; ptr<=LongEnd; ptr++)
		{
			printf("%c", *ptr);
		}
			
		printf(")\n");
	}
	else
		printf("Empty string\n");
}
int AnalizeString(char *string, char **LongBegin, char **LongEnd)
{
	char *ptr = string;
	char *WordBegin;
	int flag = 0, len = 0, maxlen=0;


	while(*ptr)
	{
		if(flag == 0 && ItSpace(*ptr) == 0)
		{
			WordBegin = ptr;
			flag = 1;
			len = 1;
		}
		else if(flag)
		{
			if(ItSpace(*ptr))
			{
				flag = 0;
				if(len > maxlen)
				{
					*LongBegin = WordBegin;
					*LongEnd = ptr - 1;
					maxlen = len;
				}
			}
			else
				len++;
		}
		ptr++;
			
	}
	
	if(flag && len > maxlen)
	{
		*LongBegin = WordBegin;
		*LongEnd = ptr - 1;
		maxlen = len;
	}

	return maxlen;

}