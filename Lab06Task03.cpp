#include <stdio.h>
#define Max 10
int DigitFromChar(char *ptr);
int Solve(char *ptr, int *mult);
int main()
{
	int mult,summ;
	char Buff[Max];

	printf("Enter the numeric string: ");
	fgets(Buff, Max, stdin);

	summ = Solve(Buff,&mult);

	printf("it's: %d \n", summ);
}

int Solve(char *ptr, int *mult)
{
	int summ;

	if(ptr[0] != '\n')
	{
		summ = Solve(ptr+1, mult) + (*mult) * DigitFromChar(ptr);
		*mult = (*mult) * 10;
		return summ;
	}
	else
	{
		*mult = 1;
		return 0;
	}
}
int DigitFromChar(char *ptr)
{
	return (*ptr) - '0';
}