#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define SIZE 20
#define TIMEEFFECT 200
void SetRange(char (*arr)[SIZE+1]);
void MakeEffect(char (*arr)[SIZE+1]);
void SetPattern(char (*arr)[SIZE+1]);
void OuputPattern(char (*arr)[SIZE+1]);
char RandomSymbol();
void delay();
int main()
{
	char arr[SIZE][SIZE+1];
	int Repeat = 0;
	
	srand(time(0));
	SetRange(arr);

	for(;Repeat<TIMEEFFECT;Repeat++)
		MakeEffect(arr);

	return 0;
}

void SetRange(char (*arr)[SIZE+1])
{
	int y=0;
	for(;y<SIZE;y++)
		arr[y][SIZE]=0;
}
void MakeEffect(char (*arr)[SIZE+1])
{
	SetPattern(arr);
	OuputPattern(arr);
	delay();
}
void SetPattern(char (*arr)[SIZE+1])
{
	int y=0,x;
	for(;y<SIZE/2;y++)
	{
		x = 0;
		for(;x<SIZE/2;x++)
		{
			arr[y][x] = RandomSymbol();
			arr[SIZE-1-y][x] = arr[y][x];
			arr[y][SIZE-1-x] = arr[y][x];
			arr[SIZE-1-y][SIZE-1-x] = arr[y][x];
		}
	}
}
void OuputPattern(char (*arr)[SIZE+1])
{
	system("cls");

	int y=0;
	for(;y<SIZE;y++)
		printf("%s\n", arr[y]);

}
char RandomSymbol()
{
	int index = rand()%2;

	return (index==0?' ':'*');
}
void delay()
{
	int start, td;

	start = clock();
	while((clock() - start)<50);

}