#include <stdio.h>
#define max 255
void DataInput(char *string);
int SetPointers(char *string, char **PointersBegin, char **PointersEnd);
void DataOutput(char **PointersBegin, char **PointersEnd, int Count);

int main()
{
	char string[max];
	char *PointersBegin[max];
	char *PointersEnd[max];
	int Count;

	DataInput(string);
	Count = SetPointers(string, PointersBegin, PointersEnd);
	DataOutput(PointersBegin, PointersEnd, Count-1);
}

void DataInput(char *string)
{
	printf("Enter string :");
	gets(string);
}


int SetPointers(char *string, char **PointersBegin, char **PointersEnd)
{
	int index=0;
	int count=0;
	int word = 0;
	char sym;

	while(string[index])
	{
		sym = string[index];
		if(sym != ' ' && word == 0)
		{
			word = 1;
			PointersBegin[count] = &string[index];
			count++;
		}
		else if(sym == ' ' && word)
		{
			word = 0;
			PointersEnd[count-1] = &string[index-1];
		}

		index++;
	}

	if(word)
		PointersEnd[count-1] = &string[index-1];

	return count;
}

void DataOutput(char **PointersBegin, char **PointersEnd, int Count)
{
	char *pointer;
	
	while(Count>=0)
	{
		printf("World N:%d - ", Count+1);

		for(pointer = PointersBegin[Count]; pointer <= PointersEnd[Count]; pointer++)
			putchar(*pointer);

		Count--;
		putchar('\n');
	}
}